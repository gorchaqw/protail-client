package main

import (
	"fmt"
	"log"
	"os"
	"protail-client/promtail"
	"time"
)

func main() {

	sourceName := "source_name"
	jobName := "job_name"
	app := "app"

	labels := fmt.Sprintf(`{source="%s", job="%s", app="%s"}`, sourceName, jobName, app)
	conf := promtail.ClientConfig{
		PushURL:            "http://localhost:3100/api/prom/push",
		Labels:             labels,
		BatchWait:          5 * time.Second,
		BatchEntriesNumber: 10000,
		SendLevel:          promtail.INFO,
		PrintLevel:         promtail.ERROR,
	}

	var (
		loki promtail.Client
		err  error
	)

	loki, err = promtail.NewClientProto(conf)
	if err != nil {
		log.Printf("promtail.NewClient: %s\n", err)
		os.Exit(1)
	}

	for i := 1; i < 10000000; i++ {
		tstamp := time.Now().String()
		loki.Debugf("source = %s time = %s, i = %d\n", sourceName, tstamp, i)
		loki.Infof("source = %s, time = %s, i = %d\n", sourceName, tstamp, i)
		loki.Warnf("source = %s, time = %s, i = %d\n", sourceName, tstamp, i)
		loki.Errorf("source = %s, time = %s, i = %d\n", sourceName, tstamp, i)
		time.Sleep(500 * time.Millisecond)
	}

	loki.Shutdown()
}
