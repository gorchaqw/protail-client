module protail-client

go 1.14

require (
	github.com/golang/protobuf v1.3.5
	github.com/golang/snappy v0.0.1
)
